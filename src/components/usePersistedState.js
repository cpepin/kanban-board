import { useEffect, useState } from 'react';

function usePersistedState(key, initialState) {
  const initialValue = localStorage[key] ? JSON.parse(localStorage[key]) : initialState;
  const [state, setState] = useState(initialValue);

  const stringifiedState = JSON.stringify(state);

  useEffect(() => {
    localStorage.setItem(key, stringifiedState);
  }, [key, stringifiedState]);

  useEffect(() => {
    const handleStorage = () => {
      const nextState = localStorage.getItem(key);
      if (nextState) {
        setState(JSON.parse(nextState));
      }
    };

    window.addEventListener('storage', handleStorage);

    return () => window.removeEventListener('storage', handleStorage);
  }, [key]);

  return [state, setState];
}

export default usePersistedState;
