import React, { memo, useCallback, useState } from 'react';

import Card from '../Card';
import EditForm from '../EditForm';
import useDraggedItem from '../useDraggedItem';

import styles from './index.module.css';

const getCenterYCoordinate = (node) => {
  return node.offsetTop + node.offsetHeight / 2;
}

const getInsertLocation = (pageY, cards) => {
  const otherCardCenters = cards
    .sort((a, b) => getCenterYCoordinate(a) - getCenterYCoordinate(b))
    .map(card => getCenterYCoordinate(card));
  
  let insertIndex = 0;
  while (insertIndex < otherCardCenters.length && pageY > otherCardCenters[insertIndex]) {
    insertIndex++;
  }

  return insertIndex;
}

const CardList = memo(({ cards, handleDragStart, handleDragEnd }) => {
  return (
    <React.Fragment>
      {cards.map(card => (
        <Card
          key={card.id}
          id={card.id}
          title={card.title}
          email={card.email}
          onDragStart={handleDragStart}
          onDragEnd={handleDragEnd}
        >
          {card.description}
        </Card>
      ))}
    </React.Fragment>
  );
});

const Column = ({ cards, onAdd, onMove, title }) => {
  const [isAdding, setIsAdding] = useState(false);
  const {
    draggedId,
    draggedFrom,
    setDraggedFrom,
    setDraggedId,
  } = useDraggedItem();

  const handleAddButtonClick = () => setIsAdding(true);
  const handleClose = useCallback(() => setIsAdding(false), [setIsAdding]);
  const handleDragStart = useCallback(
    (id) => {
      setDraggedFrom(title);
      setDraggedId(id);
    },
    [setDraggedFrom, setDraggedId, title]
  );
  const handleDragEnd = useCallback(() => setDraggedId(null), [setDraggedId]);

  const handleDrop = (e) => {
    onMove(
      title,
      draggedFrom,
      draggedId,
      getInsertLocation(e.pageY, Array.from(e.target.querySelectorAll('section')))
    );
  };
  const handleDragOver = (e) => e.preventDefault();

  return (
    <section className={styles.column} onDragOver={handleDragOver} onDrop={handleDrop}>
      <header className={styles.columnHeader}>
        <h2 className={styles.columnHeading}>{title}</h2>

        <button className={styles.addButton} onClick={handleAddButtonClick}>
          &#43;
        </button>
      </header>

      <div className={styles.columnContent}>
        <EditForm
          isVisible={isAdding}
          onClose={handleClose}
          onSubmit={onAdd}
        />

        <CardList
          cards={cards}
          handleDragStart={handleDragStart}
          handleDragEnd={handleDragEnd}
        />
      </div>
    </section>
  )
};

export default memo(Column);
