import React from 'react';
import { render } from '@testing-library/react';

import Column from './';

describe('Column', () => {
  const defaultProps = {
    onAdd: () => {},
    onMove: () => {},
    title: 'Test',
  };

  it('should display a list of cards', () => {
    const cards = [
      {
        id: 0,
        title: 'Card 0',
        email: 'test@test.com',
        description: 'Test 0',
      },
      {
        id: 1,
        title: 'Card 1',
        email: 'test@test.com',
        description: 'Test 1',
      },
    ];
    const { getAllByTestId } = render(<Column cards={cards} {...defaultProps} />);

    const domTitles = getAllByTestId('card-title').map(domTitle => domTitle.innerHTML);
    const domDescriptions = getAllByTestId('card-content').map(domDescription => domDescription.innerHTML);

    expect(
      cards
      .map(card => card.title)
      .every(title => domTitles.indexOf(title) !== -1)
    ).toBeTruthy();

    expect(
      cards
      .map(card => card.description)
      .every(description => domDescriptions.indexOf(description) !== -1)
    ).toBeTruthy();
    
  });
});