import React, { memo, useMemo, useState } from 'react';
import DraggedItemContext from './DraggedItemContext';

const DraggedItemProvider = ({ children }) => {
  const [draggedId, setDraggedId] = useState();
  const [draggedFrom, setDraggedFrom] = useState();

  // Prevent re-renders due to children
  const value = useMemo(() => ({
    draggedId,
    setDraggedId,
    draggedFrom,
    setDraggedFrom,
  }), [draggedId, setDraggedId, draggedFrom, setDraggedFrom]);

  return (
    <DraggedItemContext.Provider value={value}>
      {children}
    </DraggedItemContext.Provider>
  )
};

export default memo(DraggedItemProvider);

