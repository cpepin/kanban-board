import React, { memo, useRef } from 'react';

import Card from '../Card';

import styles from './index.module.css';

const EditForm = ({ isVisible, onClose, onSubmit }) => {
  // Didn't seem necessary to used controlled inputs here.
  const titleInput = useRef();
  const emailInput = useRef();
  const descriptionInput = useRef();

  const decoratedOnSubmit = (e) => {
    e.preventDefault();
    onClose();

    onSubmit({
      description: descriptionInput.current.value,
      email: emailInput.current.value,
      title: titleInput.current.value,
    });
  };

  return (
    <React.Fragment>
      {isVisible && (
        <Card>
          <form onSubmit={decoratedOnSubmit}>
            <div className={styles.inputGroup}>
              <label htmlFor="title" className={styles.label}>Title</label>
              <input
                ref={titleInput}
                id="title"
                name="title"
                required
                className={styles.input}
              />
            </div>

            <div className={styles.inputGroup}>
              <label htmlFor="email" className={styles.label}>Email</label>
              <input
                ref={emailInput} 
                id="email"
                name="email"
                type="email"
                required
                className={styles.input}
              />
            </div>

            <div className={styles.inputGroup}>
              <label htmlFor="description" className={styles.label}>Description</label>
              <textarea
                ref={descriptionInput}
                id="description"
                name="description"
                className={`${styles.input} ${styles.description}`}
              />
            </div>

            <div className={`${styles.inputGroup} ${styles.buttons}`}>
              <button
                type="button"
                className={styles.button}
                onClick={onClose}
              >Cancel</button>
              <button type="submit" className={`${styles.button} ${styles.buttonColored}`}>Save task</button>
            </div>
          </form>
        </Card>
      )}
    </React.Fragment>
  )
}

export default memo(EditForm);

