import React, { memo, useMemo } from 'react';
import md5 from 'md5';

import styles from './index.module.css';

const generateEmailHash = (email) => email ? md5(email.trim().toLowerCase()) : '';

const Card = ({ children, email, id, onDragStart, title, ...rest }) => {
  const handleDragStart = (e) => {
    e.dataTransfer.setData('text', '');
    onDragStart(id);
  };

  const gravatarUrl = useMemo(
    () => `https://www.gravatar.com/avatar/${generateEmailHash(email)}?s=24`,
    [email]
  );

  return (
    <section
      className={styles.card}
      draggable={true}
      onDragStart={handleDragStart}
      {...rest}
    >
      {title && (
        <>
          <header className={styles.cardHeader}>
            <img src={gravatarUrl} alt="gravatar" className={styles.gravatar} />

            <h3 className={styles.cardHeading} data-testid={'card-title'}>{title}</h3>
          </header>

          <hr className={styles.cardDivider} />
        </>
      )}

      <div className={styles.cardContent} data-testid={'card-content'}>{children}</div>
    </section>
  )
};

export default memo(Card);
