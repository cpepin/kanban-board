import { useContext } from 'react';

import DraggedItemContext from './DraggedItemContext';

function useDraggedItem() {
  return useContext(DraggedItemContext);
}

export default useDraggedItem;
