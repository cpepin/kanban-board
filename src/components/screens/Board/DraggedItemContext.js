import { createContext } from 'react';

export default createContext({
  draggedId: null,
  draggedFrom: null,
  setDraggedFrom: () => {},
  setDraggedItem: () => {},
});
