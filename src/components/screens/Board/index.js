import React, { useCallback } from 'react';

import Column from './Column';
import DraggedItemProvider from './DraggedItemProvider';

import styles from './index.module.css';
import usePersistedState from '../../usePersistedState';

const generateTodoWithId = (todo) => ({ ...todo, id: new Date().getUTCMilliseconds() });

export default function Board() {
  const [toDoItems, setToDoItems] = usePersistedState('todo_items', []);
  const [inProgressItems, setInProgressItems] = usePersistedState('in_progress_items', []);
  const [doneItems, setDoneItems] = usePersistedState('done_items', []);

  const handleTodoAdd = useCallback(
    (todo) => setToDoItems(_todoItems => [..._todoItems, generateTodoWithId(todo)]),
    [setToDoItems]
  );
  const handleInProgressAdd = useCallback(
    (todo) => setInProgressItems(_todoItems => [..._todoItems, generateTodoWithId(todo)]),
    [setInProgressItems]
  );
  const handleDoneAdd = useCallback(
    (todo) => setDoneItems(_todoItems => [..._todoItems, generateTodoWithId(todo)]),
    [setDoneItems]
  );

  const handleMove = useCallback((to, from, id, index) => {
    const getSetter = (title) => {
      switch(title) {
        case 'To do':
          return setToDoItems;
        case 'In progress':
          return setInProgressItems;
        default:
          return setDoneItems;
      }
    };

    const fromSetter = getSetter(from);
    const toSetter = getSetter(to);

    // remove
    let card;
    fromSetter(set => {
      // WARNING... not IE friendly.
      const cardIndex = set.findIndex(item => item.id === id);
      card = set[cardIndex];
      const nextSet = [...set]
      nextSet.splice(cardIndex, 1);
      return nextSet;
    });

    // add
    toSetter(set => {
      const nextSet = [...set];
      nextSet.splice(index, 0, card);
      return nextSet;
    });
  }, [setDoneItems, setInProgressItems, setToDoItems]);

  return (
    <DraggedItemProvider>
      <main className={styles.board}>
        <Column
          title={'To do'}
          cards={toDoItems}
          onAdd={handleTodoAdd}
          onMove={handleMove}
        />

        <Column
          title={'In progress'}
          cards={inProgressItems}
          onAdd={handleInProgressAdd}
          onMove={handleMove}
        />

        <Column
          title={'Done'}
          cards={doneItems}
          onAdd={handleDoneAdd}
          onMove={handleMove}
        />
      </main>
    </DraggedItemProvider>
  );
}
