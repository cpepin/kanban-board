## Outline

Here is my kanban board implementation, leveraging mostly `useState`, and a little bit of context to share information between columns. I feel pretty good about the performance, look & feel (thanks to your designer!), and general code quality. My biggest critique of this project, is that I think leveraging context was a bit overkill, and I likely could have gotten a big more clever about the code organization in the `Board.js` file.

There is a bit of duplicated code there for the add functions, and I think the `move` function is messy. I could have also created multiple move handler functions, and bound the from argument to the title of the column. It would have reduced another key from the context (which I could probably rip out entirely).

I tried to write everything by hand as a challenge. It would have been perhaps quicker if I leaned on a React drag-and-drop library, and maybe a Form library. Overall, this took closer to 3.5 hours which was a little above the time limit.

## Screenshot/GIF

(see screen_cap.gif file)

## Tests

I wrote a very simple tests that checks the display of the cards within a column. I would love to add more, but I ran out of time.

## Future iterations

- Add more tests. Would like to start leaning heavier into integration tests (maybe cypress).
- I used a `findIndex` in there. Probably should replace that with something IE friendly.
- Add better UI/UX to the active/focus states of elements. I left the browser outline in, as it's important for accessibility.
- Clean up the Board component, as I think there is some duplicate code that could be refactored. I would also maybe re-think the code organization, as hooks probably deserve their own folder.
- Could use some work on updating styling to handle mobile scenarios.
